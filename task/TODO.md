### Тестовое задание

1. Получить данные из файла data.json и вывести их на страницу как это показано на рис."пример.png".

2. Показанные на рисунке параметры находятся в узле Goods.
   "C" - цена в долларах(USD) - вывести в рублях(курс выбрать произвольно),
   "G" - id группы, 
   "T" - id товара,
   "P" - сколько единиц товара осталось (параметр, который указан в скобках в названии).

3. Сопоставления id групп и товаров с их названиями находятся в файле names.json.

4. После вывода данных навесить обработчики для добавления выбранного товара в корзину и удаления из нее. Пример корзины показан в файле "Корзина.png". Сделать рассчет общей суммы товаров и вывести отдельным полем.
5. Корзина находится на одной и той же странице вместе со списком товаров.

(\*)

6. Сделать обновление цены товара в зависимости от курса валюты.
   С интервалом в 15 секунд читать исходный файл data.json и одновременно менять курс доллара (вручную) на значение от 20 до 80, выполняя обновление данных в модели (с изменением в представлении). Если цена увеличилось в большую сторону - подсветить ячейку красным, если в меньшую - зеленым.

7. Дополнительная информация: Дизайну, показанному в примерах, следовать не обязательно. Прокомментировать основные действия. Интересные решения приветствуются.

### Сервер

php -S localhost:8080 -t server
