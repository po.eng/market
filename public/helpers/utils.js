/**
 *
 * @param {*} data группы и товары
 * @param {*} names названия
 * @param {*} rate курс валюты
 * @returns
 */
function dataJoinNames(data, names) {
  // группировка по категории
  return data.reduce((a, b) => {
    let categoryName = names[b.G].G;
    a[categoryName] = a[categoryName] ?? [];
    a[categoryName].push(b);
    return a;
  }, []);
}


function findGood(data, item) {
  return data.filter((good) => good.T == item);
}

function filterGood(data, item) {
  return data.filter((good) => good.T != item);
}
