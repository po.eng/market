function setEventListnerBasket(col) {
  document.querySelectorAll(".outBasket").forEach((outBucketButton) => {
    outBucketButton.addEventListener("click", () => {
      col(outBucketButton.value);
    });
  });
}

function setEventListnerGoods(callback) {
  document.querySelectorAll(".inBasket").forEach((inBucketButton) => {
    inBucketButton.addEventListener("click", () => {
        callback(inBucketButton.value)
    });
  });
}
