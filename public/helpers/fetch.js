// запрос попытка + задержка
async function fetchUrl(url, count = 5, delay = 3000) {
  for (let n = 0; n < count; n++) {
    try {
      return await fetch(url, {
        credentials: "include",
      });
    } catch (err) {
      console.log(err);
    }
    console.log(n, url);
    await new Promise((res) => setTimeout(res, delay));
  }
  throw new Error(`Запрос ${url} завершился неудачей после ${count} попыток`);
}

async function getData(type = "data") {
  const response = await fetchUrl(`http://45.90.34.40/?type=${type}`);
  const data = await response.json();
  return data;
}
