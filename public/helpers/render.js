function renderBucket(selector, basket, calllback) {
  let basketLayout = document.querySelector(selector);
  let basketTable = tableBasket(basket);
  basketLayout.innerHTML = card("Корзина", basketTable, "sticky-top");
  document.querySelectorAll(".outBasket").forEach((outBucketButton) => {
    outBucketButton.addEventListener("click", () => {
      if (undefined !== calllback) {
        calllback(outBucketButton.value);
      }
    });
  });
}

function renderGoods(selector, allGoods, calllback) {
  let goodsLayout = document.querySelector(selector);
  for (const category in allGoods) {
    if (Object.hasOwnProperty.call(allGoods, category)) {
      let goods = allGoods[category];
      let goodsTable = tableGoods(goods, category);
      goodsLayout.innerHTML += card(category, goodsTable);
    }
  }

  document.querySelectorAll(".inBasket").forEach((inBucketButton) => {
    inBucketButton.addEventListener("click", () => {
      if (undefined !== calllback) {
        calllback(inBucketButton.value);
      }
    });
  });
}
