function tableGoods(goods) {
  let rows = tableGoodsRows(goods);
  return tableGoodsRender(rows);
}

function tableGoodsRender(rows) {
  return ` <div class="table-responsive">
                <table class="table table-sm table-hover">
                    <thead>
                        <tr>
                            <th style="width: 2%" scope="col">#</th>
                            <th style="width: 40%" scope="col">Наименование товара</th>
                            <th style="width: 5%" scope="col-2">Цена</th>
                            <th style="width: 10%" scope="col-2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        ${rows}
                    </tbody>
                </table>
            </div>`;
}

function tableGoodsRows(goods) {
  let rows = ``;
  goods.forEach((value, index) => {
    rows += `   
      <tr>
          <th scope="row">${index}</th>
          <td >${value["N"]}</td>
          <td>$${value["C"]}</td>
          <td> 
              <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                  <button value="${value["T"]}" 
                  class="btn btn-primary btn-sm inBasket" 
                  type="button">В корзину</button>
              </div>
          </td>
      </tr>`;
  });
  return rows;
}
