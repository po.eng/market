function tableBasket(goods) {
  let rows = tableBasketRows(goods);

  let sum = goods.reduce((a, b) => a + b.CR, 0);
  let table = tableBasketRender(rows, sum);


  return table;
}

function tableBasketRender(rows = "", sum = 0) {
  return ` <div class="table-responsive">
                <table class="table table -sm table-hover ">
                  <caption class="text-end">Общаяя стоимость ${sum} руб. </caption>
                    <thead>
                        <tr>
                            <th style="width: 2%" scope="col">#</th>
                            <th style="width: 30%" scope="col">Наименование товара</th>
                            <th style="width: 5%" scope="col-2">Кол-во</th>
                            <th style="width: 6%" scope="col-2">Цена</th>
                            <th style="width: 10%" scope="col-2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        ${rows}
                    </tbody>
                </table>
            </div>`;
}

function tableBasketRows(goods) {
  let rows = ``;
  goods.forEach((value, index) => {
    rows += `   
      <tr>
          <th scope="row">${index}</th>
          <td >${value["N"]}</td>
          <td>${value["CC"]} шт.</td>
          <td>${value["CR"]} / шт.</td>
          <td> 
              <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                  <button value="${value["T"]}" 
                  class="btn btn-primary btn-sm outBasket" 
                  type="button">Удалить</button>
              </div>
          </td>
      </tr>`;
  });
  return rows;
}
